# @onekind/carousel-vue

![logo](https://gitlab.com/onekind/carousel-vue/raw/master/src/assets/logo64.png)

[![build status](https://img.shields.io/gitlab/pipeline/onekind/carousel-vue/master.svg?style=for-the-badge)](https://gitlab.com/onekind/carousel-vue.git)
[![npm-publish](https://img.shields.io/npm/dm/@onekind/carousel-vue.svg?style=for-the-badge)](https://www.npmjs.com/package/@onekind/carousel-vue)
[![release](https://img.shields.io/npm/v/@onekind/carousel-vue?label=%40onekind%2Fcarousel-vue%40latest&style=for-the-badge)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg?style=for-the-badge)](https://github.com/semantic-release/semantic-release)

A Vue carousel component

Checkout the [Demo](https://onekind.gitlab.io/carousel-vue/) which contains the component documentation.

> If you enjoy this component, feel free to drop me feedback, either via the repository, or via jose@onekind.io.

## Instalation

```bash
yarn add @onekind/carousel-vue
```

## Setting up

- Add the following to you application main.js file:

```js
import {OkCarousel} from '@onekind/carousel-vue'
import '@onekind/carousel-vue/dist/carousel-vue.css'
app.component(OkCarousel.name, OkCarousel)
```

## Customizing

You may use the `scss` files instead, which offer more customization.

- **color**: Optionally setup the color, before you import the main `scss` file.

```scss
$circle-color: rgb(0,0,255);
```

- **diameter**: Optionally setup the circle diameter, before you import the main `scss` file.

```scss
$circle-diameter: 8px;
```

- Import the main style `scss` file.

```scss
@import '@onekind/carousel-vue/src/styles/carousel.scss';
```
