import { defineConfig } from 'vite'
import { resolve } from 'path'
import vue from '@vitejs/plugin-vue'
import { visualizer } from 'rollup-plugin-visualizer'
import {name} from './package.json'
const packageName = name.replace('@', '').split('/')

export default defineConfig({
  plugins: [
    vue(),
    // https://github.com/btd/rollup-plugin-visualizer
    {...(process.env.ANALYZE && visualizer({
      open: true,
      title: name,
      template: process.env.ANALYZETEMPLATE || 'treemap',
      gzipSize: true,
    }))},
  ],
  resolve: {
    alias: {
      '~': resolve(__dirname, './src'),
      // '@': resolve(__dirname, 'src'),
      // NOTE: This line is necessary so the app doesn't break when
      // using linked external libraries
      // vue: resolve('./node_modules/vue'),
    },
  },
  build: {
    lib: {
      entry: resolve(__dirname, `src/index.js`),
      fileName: (format) => `${packageName[1]}.${format}.js`,
      // formats: ['es'],
      name: packageName[1],
    },
    rollupOptions: {
      // make sure to externalize deps that shouldn't be bundled
      // into your library
      external: ['vue'],
      output: {
        // Provide global variables to use in the UMD build
        // for externalized deps
        globals: {
          vue: 'Vue',
        },
      },
    },
  },
})
