## [1.0.1](https://gitlab.com/onekind/carousel-vue/compare/v1.0.0...v1.0.1) (2022-07-05)


### Bug Fixes

* wrong description in docs ([3e84628](https://gitlab.com/onekind/carousel-vue/commit/3e84628e84a7cca59b9a28fe396b10f05cce5c23))

# 1.0.0 (2022-07-05)


### Bug Fixes

* wrong vitepress config ([74c8bef](https://gitlab.com/onekind/carousel-vue/commit/74c8bef8fec304051de74e565d34bb22eb7bbcd8))
* wrong yarn.lock causing install issues ([99453fc](https://gitlab.com/onekind/carousel-vue/commit/99453fc10321d9df3274bb3777c642b19e13d19b))

## [1.0.2](https://gitlab.com/onekind/carousel-vue/compare/v1.0.1...v1.0.2) (2022-06-24)


### Bug Fixes

* update README.md badges ([8ddcbdc](https://gitlab.com/onekind/carousel-vue/commit/8ddcbdccb60fd2c07a7708c43efe463ab237c606))

## [1.0.1](https://gitlab.com/onekind/carousel-vue/compare/v1.0.0...v1.0.1) (2022-06-23)


### Bug Fixes

* favicon and logo not being displayed correctly in gitlab pages ([53ed550](https://gitlab.com/onekind/carousel-vue/commit/53ed5505e6ab4060cc3fa9b551b337f1bea42cd4))
* make logo and favicon base path dependant of NODE_ENV ([79cdc71](https://gitlab.com/onekind/carousel-vue/commit/79cdc710e323cdbd69d6eb9fc78ec8dadd503d2c))

# 1.0.0 (2022-06-20)


### Features

* initial commit ([78b26ac](https://gitlab.com/onekind/carousel-vue/commit/78b26ac6b00cee9e71fa595b03736ea0a41e3246))
