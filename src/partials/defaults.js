export const defaultConfigs = {
  itemsLimit: 1,
  scrollLimit: 1,
  modelValue: 0,
  transition: 300,
  autoplay: 0,
  snapAlign: 'center',
  wrapAround: false,
  pauseOnHover: false,
  mouseDrag: true,
  touchDrag: true,
  dir: 'left',
  breakpoints: undefined,
}
