const icons = {
  arrowRight: {
    vectorData: 'M8.59 16.59L13.17 12 8.59 7.41 10 6l6 6-6 6-1.41-1.41z',
    viewBox: '0 0 24 24',
    width: '1.2em',
    height: '1.2em',
    color: 'white',
    background: 'var(--ok-carousel-nav-background-color)',
    borderRadius: 'var(--ok-carousel-nav-width)',
    mainWidth: 'var(--ok-carousel-nav-width)',
    mainHeight: 'var(--ok-carousel-nav-width)',
    border: '5px solid white',
  },
  arrowLeft: {
    vectorData: 'M15.41 16.59L10.83 12l4.58-4.59L14 6l-6 6 6 6 1.41-1.41z',
    viewBox: '0 0 24 24',
    width: '1.2em',
    height: '1.2em',
    color: 'white',
    background: 'var(--ok-carousel-nav-background-color)',
    borderRadius: 'var(--ok-carousel-nav-width)',
    mainWidth: 'var(--ok-carousel-nav-width)',
    mainHeight: 'var(--ok-carousel-nav-width)',
    border: '5px solid white',
  },
};

export default icons;
