
/**
 * return a debounced version of the function
 * @param fn
 * @param delay
 */
// eslint-disable-next-line no-unused-vars
export function debounce(fn, delay) {
  let timerId
  return function (...args) {
    if (timerId) {
      clearTimeout(timerId)
    }
    timerId = setTimeout(() => {
      fn(...args)
      timerId = null
    }, delay)
  }
}

/**
 * return a throttle version of the function
 * Throttling
 *
 */
// eslint-disable-next-line no-unused-vars
export function throttle(fn, limit) {
  let inThrottle
  return function (...args) {
    const self = this
    if (!inThrottle) {
      fn.apply(self, args)
      inThrottle = true
      setTimeout(() => (inThrottle = false), limit)
    }
  }
}

export function getSlidesVNodes(vNode) {
  // Return empty array if there's any node
  if (!vNode) return []

  // Check if the Slides components are added directly without v-for (#72)
  if (vNode[0]?.type?.name === 'CarouselSlide') return vNode

  return vNode[0]?.children || []
}

export function getMaxSlideIndex(config, slidesCount) {
  if (config.wrapAround) {
    return slidesCount - 1
  }
  switch (config.snapAlign) {
    case 'start':
      return slidesCount - config.itemsLimit
    case 'end':
      return slidesCount - 1
    case 'center':
    case 'center-odd':
      return slidesCount - Math.ceil(config.itemsLimit / 2)
    case 'center-even':
      return slidesCount - Math.ceil(config.itemsLimit / 2)
    default:
      return 0
  }
}

export function getMinSlideIndex(config) {
  if (config.wrapAround) {
    return 0
  }
  switch (config.snapAlign) {
    case 'start':
      return 0
    case 'end':
      return config.itemsLimit - 1
    case 'center':
    case 'center-odd':
      return Math.floor((config.itemsLimit - 1) / 2)
    case 'center-even':
      return Math.floor((config.itemsLimit - 2) / 2)
    default:
      return 0
  }
}

export function getCurrentSlideIndex(
  config,
  val,
  max,
  min,
) {
  if (config.wrapAround) {
    return val
  }
  return Math.min(Math.max(val, min), max)
}

export function getSlidesToScroll({
  slidesBuffer,
  currentSlide,
  snapAlign,
  itemsLimit,
  wrapAround,
  slidesCount,
}) {
  let output = slidesBuffer.indexOf(currentSlide)

  if (output === -1) {
    output = slidesBuffer.indexOf(Math.ceil(currentSlide))
  }

  if (snapAlign === 'center' || snapAlign === 'center-odd') {
    output -= (itemsLimit - 1) / 2
  } else if (snapAlign === 'center-even') {
    output -= (itemsLimit - 2) / 2
  } else if (snapAlign === 'end') {
    output -= itemsLimit - 1
  }

  if (!wrapAround) {
    const max = slidesCount - itemsLimit
    const min = 0
    output = Math.max(Math.min(output, max), min)
  }
  return output
}
