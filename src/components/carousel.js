import {
  defineComponent,
  onMounted,
  onUnmounted,
  ref,
  reactive,
  provide,
  computed,
  watchEffect,
  h,
  watch,
  nextTick,
} from 'vue'

import { defaultConfigs } from '../partials/defaults'
import {
  debounce,
  throttle,
  getSlidesVNodes,
  getCurrentSlideIndex,
  getMaxSlideIndex,
  getMinSlideIndex,
  getSlidesToScroll,
} from '../partials/utils'

import Navigation from './Navigation.vue'
import Pagination from './Pagination.vue'

export default defineComponent({
  name: 'Carousel',
  props: {
    // count of items to showed per view
    itemsLimit: {
      default: defaultConfigs.itemsLimit,
      type: Number,
    },
    // count of items to be scrolled
    scrollLimit: {
      default: defaultConfigs.scrollLimit,
      type: Number,
    },
    // control infinite scrolling mode
    wrapAround: {
      default: defaultConfigs.wrapAround,
      type: Boolean,
    },
    // control snap position alignment
    snapAlign: {
      default: defaultConfigs.snapAlign,
      validator(value) {
        // The value must match one of these strings
        return ['start', 'end', 'center', 'center-even', 'center-odd'].includes(value)
      },
    },
    // sliding transition time in ms
    transition: {
      default: defaultConfigs.transition,
      type: Number,
    },
    // an object to store breakpoints
    breakpoints: {
      default: defaultConfigs.breakpoints,
      type: Object,
    },
    // time to auto advance slides in ms
    autoplay: {
      default: defaultConfigs.autoplay,
      type: Number,
    },
    // pause autoplay when mouse hover over the carousel
    pauseOnHover: {
      default: defaultConfigs.pauseOnHover,
      type: Boolean,
    },
    // slide number number of initial slide
    modelValue: {
      default: undefined,
      type: Number,
    },
    // toggle mouse dragging.
    mouseDrag: {
      default: defaultConfigs.mouseDrag,
      type: Boolean,
    },
    // toggle mouse dragging.
    touchDrag: {
      default: defaultConfigs.touchDrag,
      type: Boolean,
    },
    // control snap position alignment
    dir: {
      default: defaultConfigs.dir,
      validator(value) {
        // The value must match one of these strings
        return ['right', 'left'].includes(value)
      },
    },
    // an object to pass all settings
    settings: {
      default() {
        return {}
      },
      type: Object,
    },
    navigation: {
      type: Boolean,
      default: true,
    },
    previousIcon: {
      type: Object,
      default: () => {},
    },
    nextIcon: {
      type: Object,
      default: () => {},
    },
    pagination: {
      type: Boolean,
      default: true,
    },
  },
  setup(props, { slots, emit, expose }) {
    const root = ref(null)
    const slides = ref([])
    const slidesBuffer = ref([])
    const slideWidth = ref(0)
    const slidesCount = ref(1)
    const autoplayTimer = ref(null)
    const transitionTimer = ref(null)

    let breakpoints = ref({})

    // generate carousel configs
    let __defaultConfig = { ...defaultConfigs }
    // current config
    const config = reactive({ ...__defaultConfig })

    // slides
    const currentSlideIndex = ref(config.modelValue ?? 0)
    const prevSlideIndex = ref(0)
    const middleSlideIndex = ref(0)
    const maxSlideIndex = ref(0)
    const minSlideIndex = ref(0)

    provide('config', config)
    provide('slidesBuffer', slidesBuffer)
    provide('slidesCount', slidesCount)
    provide('currentSlide', currentSlideIndex)
    provide('maxSlide', maxSlideIndex)
    provide('minSlide', minSlideIndex)

    /**
     * Configs
     */
    function initDefaultConfigs() {
      // generate carousel configs
      const mergedConfigs = {
        ...props,
        ...props.settings,
      }

      // Set breakpoints
      breakpoints = ref({ ...mergedConfigs.breakpoints })

      // remove extra values
      __defaultConfig = { ...mergedConfigs, settings: undefined, breakpoints: undefined }

      bindConfigs(__defaultConfig)
    }

    function updateBreakpointsConfigs() {
      const breakpointsArray = Object.keys(breakpoints.value)
        .map((key) => Number(key))
        .sort((a, b) => +b - +a)
      let newConfig = { ...__defaultConfig }

      breakpointsArray.some((breakpoint) => {
        const isMatched = window.matchMedia(`(min-width: ${breakpoint}px)`).matches
        if (isMatched) {
          newConfig = {
            ...newConfig,
            ...(breakpoints.value[breakpoint]),
          }
          return true
        }
        return false
      })

      bindConfigs(newConfig)
    }

    function bindConfigs(newConfig) {
      for (let key in newConfig) {
        config[key] = newConfig[key]
      }
    }

    const handleWindowResize = debounce(() => {
      if (breakpoints.value) {
        updateBreakpointsConfigs()
        updateSlidesData()
      }
      updateSlideWidth()
    }, 16)

    /**
     * Setup functions
     */

    function updateSlideWidth() {
      if (!root.value) return
      const rect = root.value.getBoundingClientRect()
      slideWidth.value = rect.width / config.itemsLimit
    }

    function updateSlidesData() {
      slidesCount.value = Math.max(slides.value.length, 1)
      if (slidesCount.value <= 0) return

      middleSlideIndex.value = Math.ceil((slidesCount.value - 1) / 2)
      maxSlideIndex.value = getMaxSlideIndex(config, slidesCount.value)
      minSlideIndex.value = getMinSlideIndex(config)
      currentSlideIndex.value = getCurrentSlideIndex(
        config,
        currentSlideIndex.value,
        maxSlideIndex.value,
        minSlideIndex.value,
      )
    }

    function updateSlidesBuffer() {
      const slidesArray = [...Array(slidesCount.value).keys()]
      const shouldShiftSlides =
        config.wrapAround && config.itemsLimit + 1 <= slidesCount.value

      if (shouldShiftSlides) {
        const buffer =
          config.itemsLimit !== 1
            ? Math.round((slidesCount.value - config.itemsLimit) / 2)
            : 0
        let shifts = buffer - currentSlideIndex.value

        if (config.snapAlign === 'end') {
          shifts += Math.floor(config.itemsLimit - 1)
        } else if (config.snapAlign === 'center' || config.snapAlign === 'center-odd') {
          shifts++
        }

        // Check shifting directions
        if (shifts < 0) {
          for (let i = shifts; i < 0; i++) {
            slidesArray.push(Number(slidesArray.shift()))
          }
        } else {
          for (let i = 0; i < shifts; i++) {
            slidesArray.unshift(Number(slidesArray.pop()))
          }
        }
      }
      slidesBuffer.value = slidesArray
    }

    onMounted(() => {
      if (breakpoints.value) {
        updateBreakpointsConfigs()
        updateSlidesData()
      }
      nextTick(() => setTimeout(updateSlideWidth, 16))

      if (config.autoplay && config.autoplay > 0) {
        initializeAutoplay()
      }

      window.addEventListener('resize', handleWindowResize, { passive: true })
    })

    onUnmounted(() => {
      if (transitionTimer.value) {
        clearTimeout(transitionTimer.value)
      }
      resetAutoplayTimer(false)
    })

    /**
     * Carousel Event listeners
     */
    let isTouch = false
    const startPosition = { x: 0, y: 0 }
    const endPosition = { x: 0, y: 0 }
    const dragged = reactive({ x: 0, y: 0 })
    const isHover = ref(false)

    const handleMouseEnter = () => {
      isHover.value = true
    }
    const handleMouseLeave = () => {
      isHover.value = false
    }

    function handleDragStart(event) {
      isTouch = event.type === 'touchstart'

      if ((!isTouch && event.button !== 0) || isSliding.value) {
        return
      }

      startPosition.x = isTouch ? event.touches[0].clientX : event.clientX
      startPosition.y = isTouch ? event.touches[0].clientY : event.clientY

      document.addEventListener(isTouch ? 'touchmove' : 'mousemove', handleDragging, true)
      document.addEventListener(isTouch ? 'touchend' : 'mouseup', handleDragEnd, true)
    }

    const handleDragging = throttle((event) => {
      endPosition.x = isTouch ? event.touches[0].clientX : event.clientX
      endPosition.y = isTouch ? event.touches[0].clientY : event.clientY
      const deltaX = endPosition.x - startPosition.x
      const deltaY = endPosition.y - startPosition.y

      dragged.y = deltaY
      dragged.x = deltaX
    }, 16)

    function handleDragEnd() {
      const direction = config.dir === 'right' ? -1 : 1
      const tolerance = Math.sign(dragged.x) * 0.4
      const draggedSlides =
        Math.round(dragged.x / slideWidth.value + tolerance) * direction

      let newSlide = getCurrentSlideIndex(
        config,
        currentSlideIndex.value - draggedSlides,
        maxSlideIndex.value,
        minSlideIndex.value,
      )

      // Prevent clicking if there is clicked slides
      if (draggedSlides) {
        const captureClick = (e) => {
          e.stopPropagation()
          window.removeEventListener('click', captureClick, true)
        }
        window.addEventListener('click', captureClick, true)
      }

      slideTo(newSlide)

      dragged.x = 0
      dragged.y = 0

      document.removeEventListener(
        isTouch ? 'touchmove' : 'mousemove',
        handleDragging,
        true,
      )
      document.removeEventListener(isTouch ? 'touchend' : 'mouseup', handleDragEnd, true)
    }

    /**
     * Autoplay
     */
    function initializeAutoplay() {
      autoplayTimer.value = setInterval(() => {
        if (config.pauseOnHover && isHover.value) {
          return
        }

        next()
      }, config.autoplay)
    }

    function resetAutoplayTimer(restart = true) {
      if (!autoplayTimer.value) {
        return
      }

      clearInterval(autoplayTimer.value)
      if (restart) {
        initializeAutoplay()
      }
    }

    /**
     * Navigation function
     */
    const isSliding = ref(false)
    function slideTo(slideIndex, mute = false) {
      resetAutoplayTimer()

      if (currentSlideIndex.value === slideIndex || isSliding.value) {
        return
      }

      // Wrap slide index
      const lastSlideIndex = slidesCount.value - 1
      if (slideIndex > lastSlideIndex) {
        return slideTo(slideIndex - slidesCount.value)
      }
      if (slideIndex < 0) {
        return slideTo(slideIndex + slidesCount.value)
      }

      isSliding.value = true
      prevSlideIndex.value = currentSlideIndex.value
      currentSlideIndex.value = slideIndex

      if (!mute) {
        emit('update:modelValue', currentSlideIndex.value)
      }
      transitionTimer.value = setTimeout(() => {
        if (config.wrapAround) updateSlidesBuffer()
        isSliding.value = false
      }, config.transition)
    }

    function next() {
      let nextSlide = currentSlideIndex.value + config.scrollLimit
      if (!config.wrapAround) {
        nextSlide = Math.min(nextSlide, maxSlideIndex.value)
      }
      slideTo(nextSlide)
    }

    function prev() {
      let prevSlide = currentSlideIndex.value - config.scrollLimit
      if (!config.wrapAround) {
        prevSlide = Math.max(prevSlide, minSlideIndex.value)
      }
      slideTo(prevSlide)
    }
    const nav = { slideTo, next, prev }
    provide('nav', nav)

    /**
     * Track style
     */
    const slidesToScroll = computed(() =>
      getSlidesToScroll({
        slidesBuffer: slidesBuffer.value,
        itemsLimit: config.itemsLimit,
        snapAlign: config.snapAlign,
        wrapAround: config.wrapAround,
        currentSlide: currentSlideIndex.value,
        slidesCount: slidesCount.value,
      }),
    )
    provide('slidesToScroll', slidesToScroll)

    const trackStyle = computed(() => {
      const direction = config.dir === 'right' ? -1 : 1
      const xScroll = slidesToScroll.value * slideWidth.value * direction
      return {
        transform: `translateX(${dragged.x - xScroll}px)`,
        transition: `${isSliding.value ? config.transition : 0}ms`,
      }
    })

    function initCarousel() {
      initDefaultConfigs()
    }

    function restartCarousel() {
      initDefaultConfigs()
      updateBreakpointsConfigs()
      updateSlidesData()
      updateSlidesBuffer()
      updateSlideWidth()
    }

    function updateCarousel() {
      updateSlidesData()
      updateSlidesBuffer()
    }

    // Update the carousel on props change
    watch(() => Object.values(props), restartCarousel)

    // Init carousel
    initCarousel()

    watchEffect(() => {
      // Handel when slides added/removed
      const needToUpdate = slidesCount.value !== slides.value.length
      const currentSlideUpdated =
        props.modelValue !== undefined && currentSlideIndex.value !== props.modelValue

      if (currentSlideUpdated) {
        slideTo(Number(props.modelValue), true)
      }

      if (needToUpdate) {
        updateCarousel()
      }
    })

    const data = {
      config,
      slidesBuffer,
      slidesCount,
      slideWidth,
      currentSlide: currentSlideIndex,
      maxSlide: maxSlideIndex,
      minSlide: minSlideIndex,
      middleSlide: middleSlideIndex,
    }
    expose({
      updateBreakpointsConfigs,
      updateSlidesData,
      updateSlideWidth,
      updateSlidesBuffer,
      initCarousel,
      restartCarousel,
      updateCarousel,
      slideTo,
      next,
      prev,
      nav,
      data,
    })

    const slotSlides = slots.default || slots.slides
    const slotAddons = slots.addons
    const slotsProps = reactive(data)

    return () => {
      const slidesElements = getSlidesVNodes(slotSlides?.(slotsProps))
      const NavigationElement = h(
        Navigation,
        {
          previousIcon: props.previousIcon,
          nextIcon: props.nextIcon,
        },
      )
      const PaginationElement = h(
        Pagination,
        {},
      )
      slides.value = slidesElements
      // Bind slide order
      slidesElements.forEach(
        (el, index) => (el.props.index = index),
      )
      const trackEl = h(
        'ol',
        {
          class: 'ok-carousel__track',
          style: trackStyle.value,
          onMousedown: config.mouseDrag ? handleDragStart : null,
          onTouchstart: config.touchDrag ? handleDragStart : null,
        },
        slidesElements,
      )
      const viewPortEl = h('div', { class: 'ok-carousel__viewport' }, [
        trackEl,
        ...(props.navigation ? [NavigationElement] : []),
      ])

      return h(
        'section',
        {
          ref: root,
          class: {
            'ok-carousel': true,
            'ok-carousel--right': config.dir === 'right',
          },
          dir: config.dir === 'right' ? 'rtl' : 'left',
          'aria-label': 'Gallery',
          onMouseenter: handleMouseEnter,
          onMouseleave: handleMouseLeave,
        },
        [
          viewPortEl,
          ...(props.pagination ? [PaginationElement] : []),
        ],
      )
    }
  },
})
