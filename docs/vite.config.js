import { defineConfig } from 'vite'
import { resolve } from 'path'

export default defineConfig({
  resolve: {
    alias: {
      '~': resolve(__dirname, '../src'),
      // '@': resolve(__dirname, 'src'),
      // NOTE: This line is necessary so the app doesn't break when
      // using linked external libraries
      // vue: resolve('./node_modules/vue'),
    },
  },
  css: {
    preprocessorOptions: {
      scss: {
        charset: false,
        additionalData: `@import '~/styles/_variables.scss';`,
      },
    },
  },
})
