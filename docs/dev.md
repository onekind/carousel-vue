# Dev

<Basic></Basic>

<script setup>
import Basic from './guide/Basic.vue';
</script>

<style lang="scss">

.ok-carousel__item {
  min-height: 200px;
  width: 100%;
  background-color: $ok-carousel-clr-primary;
  color:  $ok-carousel-clr-white;
  font-size: 20px;
  border-radius: 8px;
  display: flex;
  justify-content: center;
  align-items: center;
}

.ok-carousel__slide {
  padding: 10px;
}

.ok-carousel__prev,
.ok-carousel__next {
  box-sizing: content-box;
}
</style>
