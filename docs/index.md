---
home: true
actionText: Getting Started →
actionLink: /getting-started
features:
- title: 🧁 Vue3
  details: Optimized to work with Vue 3 framework, not a wrapper for another library.
- title: ♿ Accessible
  details: Robust structure and Touch, Keyboard, Mouse Wheel, and Navigation support.
- title: 📱 Responsive
  details: Responsive breakpoints, to apply custom configurations for each screen size.
footer: MIT Licensed
description: A customizable accessible carousel slider optimized for Vue
meta:
  - name: og:title
    content: "@onekind/carousel-vue"
  - name: og:description
    content: A customizable accessible carousel slider optimized for Vue 3
---

## Quick Start

### Installation

```bash
yarn add @onekind/carousel-vue
```

### Basic Using

```vue
<template>
  <OkCarousel :itemsLimit="1.5">
    <OkSlide v-for="slide in 10" :key="slide">
      {{ slide }}
    </OkSlide>
  </OkCarousel>
</template>

<script>
import 'vue3-carousel/dist/carousel.css';
import { OkCarousel, OkSlide } from '@onekind/carousel-vue';

export default {
  name: 'App',
  components: {
    OkCarousel,
    OkSlide,
  },
};
</script>
```
