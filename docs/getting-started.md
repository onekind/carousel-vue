# Getting started

## Installation

```bash
yarn add @onekind/carousel-vue
```

## Basic Using

```vue
<template>
  <OkCarousel :itemsLimit="1.5">
    <OkSlide v-for="slide in 10" :key="slide">
      {{ slide }}
    </OkSlide>
  </OkCarousel>
</template>

<script setup>
import 'vue3-carousel/dist/carousel.css';
import { OkCarousel, OkSlide } from '@onekind/carousel-vue';
</script>
```

## Available Props

| Prop           | Default  | Description                                                      |
|----------------|----------|------------------------------------------------------------------|
| `itemsLimit`   | `1`      | items to showed per view (can be a fraction)                     |
| `scrollLimit`  | `1`      | slides to be scrolled                                            |
| `wrapAround`   | `false`  | infinite scrolling mode                                          |
| `snapAlign`    | `center` | position alignment, values `start`, `end`, `center[-odd\|-even]` |
| `transition`   | `300`    | sliding transition time in ms.                                   |
| `autoplay`     | `0`      | Auto play time in ms.                                            |
| `settings`     | `{}``    | an object to pass all settings.                                  |
| `breakpoints`  | `null`   | an object to pass all the breakpoints settings.                  |
| `modelValue`   | `0`      | index number of the initial slide.                               |
| `mouseDrag`    | `true`   | toggle mouse dragging                                            |
| `touchDrag`    | `true`   | toggle pointer touch dragging                                    |
| `pauseOnHover` | `true`   | toggle if auto play should pause on mouse hover                  |
| `dir`          | `left`   | carousel direction. Available values `left`, `right`             |
| `navigation`   | `true`   | disable navigation by setting to `false`                         |
| `previousIcon` | `true`   | icon object to use instead of the default icon                   |
| `nextIcon`     | `{}`     | icon object to use instead of the default icon                   |
| `pagination`   | `{}`     | disable pagination by setting to `false`                         |

## Slots

### Slides/Default

Used to render the carousel items. You can use either the default slot or wrap element in `slides` slot.

```vue
<OkCarousel>
  <template #slides>
    <OkSlide v-for="slide in 10" :key="slide">
      ...
    </OkSlide>
  </template>
</OkCarousel>
```

### Addons

Used to add display carousel addons components.

```vue
<OkCarousel>
  ...
</OkCarousel>
```

### Slots Attributes

| Prop           | Description                          |
|----------------|--------------------------------------|
| `slideWidth`   | the width of a single slide element. |
| `currentSlide` | index number of the current slide.   |
| `slidesCount`  | the count of all slides              |

#### Example

```vue {6,7,8}
<OkCarousel>
  <OkSlide v-for="slide in slides" :key="slide">
    <div class="ok-carousel__item">{{ slide }}</div>
  </OkSlide>
</OkCarousel>
```

<script setup>
import Badge from './.vitepress/components/Badge.vue';
</script>
