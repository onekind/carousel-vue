# Examples

## [Basic](https://gitlab.com/onekind/carousel-vue/raw/master/docs/guide/Basic.vue)

<Basic />

<<< @/guide/Basic.vue

## [Wrap Around](https://gitlab.com/onekind/carousel-vue/raw/master/docs/guide/WrapAround.vue)

<WrapAround />

<<< @/guide/WrapAround.vue

## [Breakpoints](https://gitlab.com/onekind/carousel-vue/raw/master/docs/guide/Breakpoints.vue)

<Breakpoints />

<<< @/guide/Breakpoints.vue

## [HiddenArrows](https://gitlab.com/onekind/carousel-vue/raw/master/docs/guide/HiddenArrows.vue)

<HiddenArrows />

<<< @/guide/HiddenArrows.vue

## [Pagination Disabled](https://gitlab.com/onekind/carousel-vue/raw/master/docs/guide/PaginationDisabled.vue)

<PaginationDisabled />

<<< @/guide/PaginationDisabled.vue

## [Navigation Disabled](https://gitlab.com/onekind/carousel-vue/raw/master/docs/guide/NavigationDisabled.vue)

<NavigationDisabled />

<<< @/guide/NavigationDisabled.vue

## [Autoplay](https://gitlab.com/onekind/carousel-vue/raw/master/docs/guide/Autoplay.vue)

<Autoplay />

<<< @/guide/Autoplay.vue

## [Active Classes](https://gitlab.com/onekind/carousel-vue/raw/master/docs/guide/ActiveClasses.vue)

<ActiveClasses />

<<< @/guide/ActiveClasses.vue

## [Custom Navigation](https://gitlab.com/onekind/carousel-vue/raw/master/docs/guide/CustomNavigation.vue)

<CustomNavigation />

<<< @/guide/CustomNavigation.vue

<script setup>
import Basic from './guide/Basic.vue'
import WrapAround from './guide/WrapAround.vue'
import Breakpoints from './guide/Breakpoints.vue'
import HiddenArrows from './guide/HiddenArrows.vue'
import PaginationDisabled from './guide/PaginationDisabled.vue'
import NavigationDisabled from './guide/NavigationDisabled.vue'
import Autoplay from './guide/Autoplay.vue'
import ActiveClasses from './guide/ActiveClasses.vue'
import CustomNavigation from './guide/CustomNavigation.vue'
</script>

<style>
.ok-carousel__item {
  min-height: 200px;
  width: 100%;
  background-color: $ok-carousel-clr-primary;
  color:  $ok-carousel-clr-white;
  font-size: 20px;
  border-radius: 8px;
  display: flex;
  justify-content: center;
  align-items: center;
}

.ok-carousel__slide {
  padding: 10px;
}

.ok-carousel__prev,
.ok-carousel__next {
  box-sizing: content-box;
}
</style>
