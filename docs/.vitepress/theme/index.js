import DefaultTheme from 'vitepress/theme'
import '~/styles/carousel.scss'
import components from '~/index.js'
import './theme.scss'

export default {
  ...DefaultTheme,
  enhanceApp({ app }) {
    app.use(components)
  },
}
