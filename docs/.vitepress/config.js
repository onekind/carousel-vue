import { resolve, join } from 'path'
import dirTree from 'directory-tree'

// NOTE: The order of these pages is also the order
// of appearance in the navigation
/* const pages = [
  'Components',
] */
// const projects = dirTree(join(__dirname, '../guide'), { extensions: /\.md/ })
const isProd = process.env.NODE_ENV === 'production'

const setLink = (name, path) => {
  const currentPath = resolve(__dirname, '../')
  let link = path.replace(`${currentPath}`, '')
  link = link.replace(/\\/g, '/')
  link = `${link}/`
  return link
}

const navigation = () => {
  const nav = []
  pages.forEach((page) => {
    const content = projects.children.find(
      ({ name }) => name === page.toLowerCase(),
    )?.children

    nav.push({
      text: page,
      items: content.map(({ name, path }) => ({
        text: name.replace(/-/g, ' '),
        link: setLink(name, path),
      })),
    })
  })
  return nav
}

export default {
  head: [
    ['link', { rel: 'icon', type: 'image/x-icon', href: `${isProd ? '/carousel-vue' : ''}/favicon.ico` }],
  ],
  lang: 'en-US',
  base: '/carousel-vue/',
  title: ' ',
  description: 'A Vue carousel component',
  outDir: '../public',
  themeConfig: {
    docsDir: 'docs',
    logo: `${isProd ? '/carousel-vue' : ''}/logo.png`,
    sidebar: [
      {
        text: 'General',
        items: [
          { text: 'Home', link: '/' },
          { text: 'Getting Started', link: '/getting-started' },
          { text: 'Guide', link: '/guide' },
          { text: 'API', link: '/api' },
        ],
      },
      /* ...navigation(), */
    ],
  },
}
